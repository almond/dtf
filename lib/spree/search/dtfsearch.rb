module Spree::Search
	class Dtf < Spree::Core::Search::Base
		def retrieve_products
			@products_scope = get_base_scope
			curr_page = page || 1

			@products = @products_scope.includes([:master => :prices])
			unless Spree::Config.show_products_without_price
				@products = @products.where("spree_prices.amount IS NOT NULL").where("spree_prices.currency" => current_currency)
			end
			@products = @products.page(curr_page).per(per_page)
		end

		protected
		def get_base_scope
			base_scope = Spree::Product.active
			base_scope = base_scope.in_taxon(taxon) unless taxon.blank?

			# Modified. Delete
			base_scope = base_scope.in_brand(brand) unless brand.blank?
			base_scope = base_scope.in_category(category) unless category.blank?
			#base_scope = base_scope.in_catbrand(category, brand) unless category.blank or brand.blank?
			# End modified

			base_scope = get_products_conditions_for(base_scope, keywords) unless keywords.blank?
			base_scope = add_search_scopes(base_scope)
			base_scope
		end

		def prepare(params)
			@properties[:taxon] = params[:taxon].blank? ? nil : Spree::Taxon.find(params[:taxon])
			@properties[:keywords] = params[:keywords]
			@properties[:search] = params[:search]

			@properties[:brand] = params[:brand].blank? ? nil : Spree::Taxon.find(params[:brand])
			@properties[:category] = params[:category].blank? ? nil : Spree::Taxon.find(params[:category])

			per_page = params[:per_page].to_i
			@properties[:per_page] = per_page > 0 ? per_page : Spree::Config[:products_per_page]
			@properties[:page] = (params[:page].to_i <= 0) ? 1 : params[:page].to_i
		end
	end
end
