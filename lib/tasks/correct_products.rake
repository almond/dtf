require "pry"
require "csv"
namespace :dtf do
	desc "Adds additional information about the products that has not been entered"
	task :correct_products => [:environment] do
		CSV.foreach("#{Rails.root}/public/csv/lsfs_corrected.csv",
			:headers => true,
			:header_converters => :symbol) do |line|
			data = line.to_hash
			product = Spree::Variant.where(:sku => data[:lsfs_item_code]).first
			if not product.nil?
				product = product.product
				puts product.name
				product.set_property("directions", data[:directions])
				product.set_property("allergen_information", data[:allergen_information])
				product.set_property("ingredients", data[:ingredients])
				puts "#{product.name} has been successfully updated"
			else
				puts "Product missing.. for some reason"
			end
		end
	end

	desc "Adds correct images to Spree products"
	task :add_images => [:environment] do 
		filepath = "#{Rails.root}/public/csv/image/"
		CSV.foreach("#{Rails.root}/public/csv/lsfs_corrected.csv", 
							 :headers => true, 
							 :header_converters => :symbol) do |line|
				data = line.to_hash
				product = Spree::Variant.where(:sku => data[:lsfs_item_code]).first

				image_path = filepath + data[:large_image]
				if File.exists?(image_path)
					if not product.nil?
						puts product.sku
						file = File.open(image_path)
						image = Spree::Image.new(
							{:attachment => file,
							:viewable => product}, :without_protection => true
						)
						image.save
					end
				else
					puts "File does not exit"
				end
		end
	end

	desc "Change prices to that weird Shawn thing"
	task :price_increase => [:environment] do
		products = Spree::Product.all
		products.each do |product|
			puts "#{product.sku} is being modified"
			puts "Old price: $#{product.price}"
			puts "Wholesale price: $#{product.cost_price}"
			if product.cost_price
				product.price = product.cost_price * 1.15
				puts "New price: $#{product.price}"
			else
				puts "Price not changed"
			end
			puts " "
			product.save
		end
	end


	desc "Update stock numbers on products"
	task :update_stock_numbers => [:environment]do
		filepath = "#{Rails.root}/public/csv/lsfs_corrected.csv"
		CSV.foreach(filepath, :headers => true, :header_converters => :symbol) do |line|
			data = line.to_hash
			variant = Spree::Variant.where(:sku => data[:lsfs_item_code]).first

			# Now we need the retarded addition of different stock groups
			total_stock = 0
			total_stock += data[:ga_quantity].to_i
			total_stock += data[:ct_quantity].to_i
			total_stock += data[:fl_quantity].to_i
			total_stock += data[:tx_quantity].to_i

			if not variant.nil?
				stock = variant.stock_items.first
				stock.adjust_count_on_hand(total_stock)
				stock.save

				puts "#{variant.sku} was sucessfully updated"
			end

		end
	end

	desc "Add products to relevant shipping categories"
	task :add_shipping_categories => [:environment] do
		products = Spree::Product.all

		small = Spree::ShippingCategory.where(:name => "small").first
		medium = Spree::ShippingCategory.where(:name => "medium").first
		large = Spree::ShippingCategory.where(:name => "large").first

		products.each do |product|
			puts "#{product.sku} is being worked on"
			if not product.weight.nil?
				if product.weight.to_f < 0.5
					product.shipping_category = small
				elsif product.weight.to_f > 0.5 and product.weight.to_f < 1.3
					product.shipping_category = medium
				else
				  product.shipping_category = large
				end
			else
				product.weight = 1.5
				product.shipping_category = large
			end
			product.save
		end
	end

	desc "Correct product price... except tshirts and apparel"
	task :correct_product_price => [:environment] do
		products = Spree::Product.all
		products.each do |product|
			taxons = product.taxons
			taxons.each do |taxon|
				if taxon.id != 176
					puts product.name
					product.price = product.cost_price + 2.00
					product.save
				end
			end
		end
	end
end
