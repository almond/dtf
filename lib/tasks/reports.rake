namespace :dtf do
	desc "Generates a tshirt report on console"
	task :get_tee_report => [:environment] do 
		product = Spree::Product.find(2110)
		# Now get every order
		orders = Spree::Order.joins(:line_items).where(:spree_line_items => { :variant_id => product.variants })

		puts "Order ID, Email, Address, Size"
		orders.each do |order|
			puts "#{order.number}, #{order.email}, #{order.ship_address}, #{order.line_items[0].variant.options_text}"
		end
	end
end
