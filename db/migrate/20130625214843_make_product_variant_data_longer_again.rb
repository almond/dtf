class MakeProductVariantDataLongerAgain < ActiveRecord::Migration
	def change
		change_column :spree_product_properties, :value, :text, :limit => nil
	end
end
