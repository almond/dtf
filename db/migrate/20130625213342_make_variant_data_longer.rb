class MakeVariantDataLonger < ActiveRecord::Migration
	def change
		change_column :spree_properties, :presentation, :text, :limit => nil
	end
end
