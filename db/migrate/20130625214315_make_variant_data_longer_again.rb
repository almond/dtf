class MakeVariantDataLongerAgain < ActiveRecord::Migration
	def change
		change_column :spree_properties, :presentation, :text
	end
end
