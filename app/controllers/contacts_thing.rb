class ContactsController < ApplicationController

	def test 
		redirect_to(root_url)
	end

	def index		
		# Just send a fucking email somewhere
		# and redirect
		ContactMailer.deliver_send_contact_form(params)
		respond_to do |format|
			format.html {redirect_to(root_path, :notice => "Your email has been sent. Thank you")}
		end
	end

end
