class ContactsController < ApplicationController
	def send_contact_email
		# Just send a fucking email somewhere
		# and redirect
		ContactMailer.send_contact_form(params).deliver
		respond_to do |format|
			format.html {redirect_to("/", :notice => "Your email has been sent. Thank you")}
		end
	end
end
