Spree::Product.class_eval do
	# Add new search shit
	add_search_scope :in_taxon do |taxon|
		if ActiveRecord::Base.connection.adapter_name == "PostgreSQL"
			scope = select("DISTINCT ON (spree_products.id) spree_products.*")
		else
			scope = select("DISTINCT(spree_products.id), spree_products.*")
		end

		scope.joins(:taxons).
		where(Spree::Taxon.table_name => { :id => taxon.self_and_descendants.map(&:id) })
	end


	add_search_scope :in_catbrand do |category, brand|
		if ActiveRecord::Base.connection.adapter_name == "PostgreSQL"
			scope = select("DISTINCT ON (spree_products.id) spree_products.*")
		else
			scope = select("DISTINCT(spree_products.id), spree_products.*")
		end

		scope.joins(:taxons).
			where(Spree::Taxon.table_name => { :id => [category.id, brand.id] })
	end

	add_search_scope :in_category do |category|
		if ActiveRecord::Base.connection.adapter_name == "PostgreSQL"
			scope = select("DISTINCT ON (spree_products.id) spree_products.*")
		else
			scope = select("DISTINCT(spree_products.id), spree_products.*")
		end

		scope.joins(:taxons).
			where(Spree::Taxon.table_name => { :id => category.id } )
	end

	add_search_scope :in_brand do |brand|
		if ActiveRecord::Base.connection.adapter_name == "PostgreSQL"
			scope = select("DISTINCT ON (spree_products.id) spree_products.*")
		else
			scope = select("DISTINCT(spree_products.id), spree_products.*")
		end

		scope.joins(:taxons).
			where(Spree::Taxon.table_name => { :id => brand.id } )
	end
end
