// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require store/lib
//= require store/modernizr
//= require store/easing
//= require store/bs
//= require store/bxslider
//= require store/input-clear
//= require store/range-slider
//= require store/jquery.zoom
//= require store/bookblock.js
//= require store/custom.js
//= require store/social.js
//= require store/spree_frontend
//= require store/cart
